<?php
/**
 * @author Shaun Stone
 * Date: 08/06/2015
 * Time: 10:46
 */

namespace Smks\BBC;

class APIFake
{
    private $fixture = <<<JSON
{
    "version": "1.0",
    "schema": "/ibl/v1/schema/ibl.json",
    "atoz_programmes": {
        "character": "c",
        "count": 64,
        "page": 1,
        "per_page": 20,
        "elements": [
            {
                "id": "p02pxd11",
                "initial_children": [
                    {
                        "id": "p02rnb92",
                        "type": "episode",
                        "title": "Cadw Cwmni gyda John Hardy",
                        "lexical_sort_letter": "C",
                        "synopses": {
                            "small": "Malan Wilkinson sy'n sgwrsio am ei hamser yn uned Hergest yn ystod pennod dywyll iawn y...",
                            "medium": "Malan Wilkinson sy'n sgwrsio am ei hamser yn uned Hergest yn ystod pennod dywyll iawn yn ei bywyd. Malan Willkinson discusses bi-polar disorder and we hear more about the Welsh ...",
                            "large": "Malan Wilkinson sy'n sgwrsio am ei hamser yn uned Hergest yn ystod pennod dywyll iawn yn ei bywyd. Malan Willkinson discusses bi-polar disorder and we hear more about the Welsh in Patagonia."
                        },
                        "tleo_id": "p02pxd11",
                        "tleo_type": "series",
                        "images": {
                            "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p02rtq6b.jpg",
                            "type": "image"
                        },
                        "guidance": false,
                        "related_links": [],
                        "subtitle": "Pennod 8",
                        "release_date": "28 May 2015",
                        "parent_id": "p02pxd11",
                        "master_brand": {
                            "id": "s4cpbs",
                            "attribution": "s4c",
                            "titles": {
                                "small": "S4C",
                                "medium": "S4C",
                                "large": "S4C"
                            }
                        },
                        "versions": [
                            {
                                "type": "version",
                                "id": "p02s2v8w",
                                "hd": false,
                                "download": true,
                                "availability": {
                                    "start": "2015-05-28T21:00:00Z",
                                    "end": "2015-06-27T21:00:00Z",
                                    "remaining": {
                                        "text": "Available for 19 days"
                                    }
                                },
                                "kind": "technical-replacement",
                                "duration": {
                                    "value": "PT23M58S",
                                    "text": "24 mins"
                                },
                                "first_broadcast": "28 May 2015"
                            },
                            {
                                "type": "version",
                                "id": "p02rnb9c",
                                "hd": false,
                                "download": true,
                                "availability": {
                                    "start": "2015-05-28T21:00:00Z",
                                    "end": "2015-06-27T21:00:00Z",
                                    "remaining": {
                                        "text": "Available for 19 days"
                                    }
                                },
                                "kind": "original",
                                "duration": {
                                    "value": "PT23M58S",
                                    "text": "24 mins"
                                },
                                "first_broadcast": "28 May 2015"
                            }
                        ],
                        "status": "available"
                    }
                ],
                "tleo_type": "series",
                "type": "programme_large",
                "title": "Cadw Cwmni gyda John Hardy",
                "synopses": {
                    "small": "Sioe sgwrsio yng nghwmni John Hardy a'i westeion. John Hardy chats to guests about even...",
                    "medium": "Sioe sgwrsio yng nghwmni John Hardy a'i westeion. John Hardy chats to guests about events in their lives.",
                    "large": "Sioe sgwrsio yng nghwmni John Hardy a'i westeion. John Hardy chats to guests about events in their lives."
                },
                "status": "available",
                "images": {
                    "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p02q9szm.jpg",
                    "type": "image"
                },
                "lexical_sort_letter": "C",
                "master_brand": {
                    "id": "s4cpbs",
                    "attribution": "s4c",
                    "titles": {
                        "small": "S4C",
                        "medium": "S4C",
                        "large": "S4C"
                    }
                },
                "count": 3
            },
            {
                "id": "b00vr6ty",
                "initial_children": [
                    {
                        "id": "b03zk7xg",
                        "type": "episode",
                        "title": "Cagney and Lacey",
                        "lexical_sort_letter": "C",
                        "synopses": {
                            "small": "Mary Beth is visited by a face from the past.",
                            "medium": "Christine takes on the investigation of a West Point cadet accused of possessing cocaine. With her new baby home, Mary Beth is visited by a face from her past.",
                            "large": "Christine takes on the investigation of Andrew Brennan, an outstanding West Point cadet accused of possessing cocaine. With their new baby home, Harvey seeks to protect Mary Beth from a face from her past."
                        },
                        "tleo_id": "b00vr6ty",
                        "tleo_type": "brand",
                        "images": {
                            "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p01tlzc8.jpg",
                            "type": "image",
                            "inherited_from": "brand"
                        },
                        "guidance": false,
                        "related_links": [],
                        "subtitle": "Series 5: 17. Post-Partum",
                        "release_date": "17 Feb 1986",
                        "parent_id": "b01pmdt8",
                        "master_brand": {
                            "id": "bbc_two",
                            "ident_id": "p00pvbl8",
                            "attribution": "bbc_two",
                            "titles": {
                                "small": "BBC Two",
                                "medium": "BBC Two",
                                "large": "BBC Two"
                            }
                        },
                        "versions": [
                            {
                                "type": "version",
                                "id": "b0002jfk",
                                "hd": false,
                                "download": false,
                                "availability": {
                                    "start": "2015-06-05T12:48:16Z",
                                    "end": "2015-06-12T12:45:00Z",
                                    "remaining": {
                                        "text": "Available until Fri 1:45pm"
                                    }
                                },
                                "kind": "original",
                                "duration": {
                                    "value": "PT50M",
                                    "text": "50 mins"
                                },
                                "first_broadcast": "17 Feb 1986"
                            }
                        ],
                        "status": "available"
                    }
                ],
                "tleo_type": "brand",
                "type": "programme_large",
                "title": "Cagney and Lacey",
                "synopses": {
                    "small": "Drama series about two New York women police detectives.",
                    "medium": "Detective series, featuring the two first ladies of the New York Police Department, Christine Cagney and Mary Beth Lacey."
                },
                "status": "available",
                "images": {
                    "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p01tlzc8.jpg",
                    "type": "image"
                },
                "lexical_sort_letter": "C",
                "master_brand": {
                    "id": "bbc_two",
                    "ident_id": "p00pvbl8",
                    "attribution": "bbc_two",
                    "titles": {
                        "small": "BBC Two",
                        "medium": "BBC Two",
                        "large": "BBC Two"
                    }
                },
                "count": 3
            },
            {
                "id": "p02mbg0z",
                "initial_children": [
                    {
                        "id": "p02mbgs3",
                        "type": "episode",
                        "title": "Call My Bluff",
                        "lexical_sort_letter": "C",
                        "synopses": {
                            "small": "Celebrities try to deceive their opponents about the definitions of obscure words. (1999)",
                            "medium": "Celebrities try to deceive their opponents about the definitions of obscure words. Hosted by Bob Holness. (1999)",
                            "large": "First transmitted in 1999, the popular panel game of word definitions and deceptions is hosted by Bob Holness. \n\n\n\nTwo teams of celebrity contestants take it in turn to provide three definitions of an obscure word, only one of which is correct. The other team then has to guess the correct definition.\n\n\n\nIn this episode captains Alan Coren and Sandi Toksvig are joined by Lynda Bellingham, Paul Rankin, Sheila Hancock and Simon Callow."
                        },
                        "tleo_id": "p02mbg0z",
                        "tleo_type": "brand",
                        "images": {
                            "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p02ns7rx.jpg",
                            "type": "image"
                        },
                        "guidance": false,
                        "related_links": [
                            {
                                "id": "p02p4w67",
                                "type": "link",
                                "kind": "priority_content",
                                "title": "More BBC Four Collections",
                                "url": "http://www.bbc.co.uk/bbcfour/collections"
                            }
                        ],
                        "subtitle": "19/11/1999",
                        "release_date": "19 Nov 1999",
                        "parent_id": "p02mbg0z",
                        "master_brand": {
                            "id": "bbc_webonly",
                            "attribution": "bbc",
                            "titles": {
                                "small": "BBC",
                                "medium": "BBC",
                                "large": "BBC"
                            }
                        },
                        "versions": [
                            {
                                "type": "version",
                                "id": "p02mpkkn",
                                "hd": true,
                                "download": false,
                                "availability": {
                                    "start": "2015-04-15T11:00:00Z",
                                    "remaining": {
                                        "text": "Available for over a year"
                                    }
                                },
                                "kind": "iplayer-version",
                                "duration": {
                                    "value": "PT28M28S",
                                    "text": "28 mins"
                                },
                                "first_broadcast": "19 Nov 1999"
                            }
                        ],
                        "status": "available",
                        "labels": {
                            "editorial": "Archive"
                        }
                    }
                ],
                "tleo_type": "brand",
                "type": "programme_large",
                "title": "Call My Bluff",
                "synopses": {
                    "small": "Game show between two teams of three celebrity contestants.",
                    "medium": "Game show between two teams of three celebrity contestants.",
                    "large": "Game show in which two teams of three celebrity contestants take turns to provide definitions of an obscure word, and try to guess which one is correct."
                },
                "status": "available",
                "images": {
                    "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p02mbg95.jpg",
                    "type": "image"
                },
                "lexical_sort_letter": "C",
                "master_brand": {
                    "id": "bbc_two",
                    "ident_id": "p00pvbl8",
                    "attribution": "bbc_two",
                    "titles": {
                        "small": "BBC Two",
                        "medium": "BBC Two",
                        "large": "BBC Two"
                    }
                },
                "count": 1
            },
            {
                "id": "b00drz1t",
                "initial_children": [
                    {
                        "id": "b0109l7p",
                        "type": "episode",
                        "title": "Calum Clachair: Gleann na Grèine/Bob the Builder: Project Fix It",
                        "lexical_sort_letter": "C",
                        "synopses": {
                            "small": "Lofty builds a big shelter.",
                            "medium": "Gaelic children's animation. Lofty builds a big shelter.",
                            "large": "Gaelic children's animation. Lofty builds a big shelter."
                        },
                        "tleo_id": "b00drz1t",
                        "tleo_type": "brand",
                        "images": {
                            "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p01l611b.jpg",
                            "type": "image",
                            "inherited_from": "series"
                        },
                        "guidance": false,
                        "related_links": [],
                        "subtitle": "Series 1: 2. Càit an tèid Cailean?/Where is Lofty's Shelter?",
                        "release_date": "26 Feb 2008",
                        "parent_id": "b00tm26n",
                        "master_brand": {
                            "id": "bbc_alba",
                            "ident_id": "p00pvbgm",
                            "attribution": "bbc_alba",
                            "titles": {
                                "small": "BBC ALBA",
                                "medium": "BBC ALBA",
                                "large": "BBC ALBA"
                            }
                        },
                        "versions": [
                            {
                                "type": "version",
                                "id": "b0109khn",
                                "hd": false,
                                "download": true,
                                "availability": {
                                    "start": "2015-06-02T17:10:52Z",
                                    "end": "2015-06-11T16:30:00Z",
                                    "remaining": {
                                        "text": "Available until Thu 5:30pm"
                                    }
                                },
                                "kind": "original",
                                "duration": {
                                    "value": "PT10M",
                                    "text": "10 mins"
                                },
                                "first_broadcast": "8:50am 26 Feb 2008"
                            }
                        ],
                        "status": "available"
                    }
                ],
                "tleo_type": "brand",
                "type": "programme_large",
                "title": "Calum Clachair: Gleann na Grèine/Bob the Builder: Project Fix It",
                "synopses": {
                    "small": "Animated adventures of builder Bob and his friends",
                    "medium": "Animated adventures of builder Bob and his friends"
                },
                "status": "available",
                "images": {
                    "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p01lcpk9.jpg",
                    "type": "image"
                },
                "lexical_sort_letter": "C",
                "master_brand": {
                    "id": "bbc_alba",
                    "ident_id": "p00pvbgm",
                    "attribution": "bbc_alba",
                    "titles": {
                        "small": "BBC ALBA",
                        "medium": "BBC ALBA",
                        "large": "BBC ALBA"
                    }
                },
                "count": 1
            },
            {
                "id": "b05x7t27",
                "initial_children": [
                    {
                        "id": "b05x7t27",
                        "type": "episode",
                        "title": "Cannes 2015",
                        "lexical_sort_letter": "C",
                        "synopses": {
                            "small": "A look back at some of the highlights of the 2015 Cannes Film Festival.",
                            "medium": "A look back at some of the highlights of the 2015 Cannes Film Festival. The programme features reports and interviews with some of the international stars and top directors."
                        },
                        "tleo_id": "b05x7t27",
                        "tleo_type": "episode",
                        "images": {
                            "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p02rr2dv.jpg",
                            "type": "image"
                        },
                        "guidance": false,
                        "related_links": [],
                        "release_date": "23 May 2015",
                        "master_brand": {
                            "id": "bbc_news24",
                            "ident_id": "p00pvbhy",
                            "attribution": "bbc_news24",
                            "titles": {
                                "small": "BBC News",
                                "medium": "BBC News",
                                "large": "BBC News Channel"
                            }
                        },
                        "versions": [
                            {
                                "type": "version",
                                "id": "b05x7t14",
                                "hd": true,
                                "download": true,
                                "availability": {
                                    "start": "2015-05-23T01:01:25Z",
                                    "end": "2015-06-25T21:00:00Z",
                                    "remaining": {
                                        "text": "Available for 17 days"
                                    }
                                },
                                "kind": "original",
                                "duration": {
                                    "value": "PT30M",
                                    "text": "30 mins"
                                },
                                "first_broadcast": "12:30am 23 May 2015"
                            }
                        ],
                        "status": "available"
                    }
                ],
                "tleo_type": "episode",
                "type": "programme_large",
                "title": "Cannes 2015",
                "synopses": {
                    "small": "A look back at some of the highlights of the 2015 Cannes Film Festival.",
                    "medium": "A look back at some of the highlights of the 2015 Cannes Film Festival. The programme features reports and interviews with some of the international stars and top directors."
                },
                "status": "available",
                "images": {
                    "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p02rr2dv.jpg",
                    "type": "image"
                },
                "lexical_sort_letter": "C",
                "master_brand": {
                    "id": "bbc_news24",
                    "ident_id": "p00pvbhy",
                    "attribution": "bbc_news24",
                    "titles": {
                        "small": "BBC News",
                        "medium": "BBC News",
                        "large": "BBC News Channel"
                    }
                },
                "count": 1
            },
            {
                "id": "p02f7d95",
                "initial_children": [
                    {
                        "id": "p02hclgp",
                        "type": "episode",
                        "title": "Cartrefi Cefn Gwlad Cymru",
                        "lexical_sort_letter": "C",
                        "synopses": {
                            "small": "Bydd Aled Samuel yn olrhain cyfraniad y bwthyn i'n treftadaeth bensaernïol. Aled Samuel...",
                            "medium": "Bydd Aled Samuel yn olrhain cyfraniad y bwthyn i'n treftadaeth bensaernïol. Aled Samuel traces the contribution the cottage has made to our architectural heritage.",
                            "large": "Bydd Aled Samuel yn olrhain cyfraniad y bwthyn i'n treftadaeth bensaernïol. Aled Samuel traces the contribution the cottage has made to our architectural heritage."
                        },
                        "tleo_id": "p02f7d95",
                        "tleo_type": "series",
                        "images": {
                            "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p02hrx1m.jpg",
                            "type": "image"
                        },
                        "guidance": false,
                        "related_links": [],
                        "subtitle": "Y Bwthyn",
                        "release_date": "24 Nov 2010",
                        "parent_id": "p02f7d95",
                        "master_brand": {
                            "id": "s4cpbs",
                            "attribution": "s4c",
                            "titles": {
                                "small": "S4C",
                                "medium": "S4C",
                                "large": "S4C"
                            }
                        },
                        "versions": [
                            {
                                "type": "version",
                                "id": "p02rbptr",
                                "hd": false,
                                "download": false,
                                "availability": {
                                    "start": "2015-05-19T15:00:00Z",
                                    "end": "2015-06-18T15:00:00Z",
                                    "remaining": {
                                        "text": "Available for 10 days"
                                    }
                                },
                                "kind": "technical-replacement",
                                "duration": {
                                    "value": "PT47M47S",
                                    "text": "48 mins"
                                },
                                "first_broadcast": "24 Nov 2010"
                            },
                            {
                                "type": "version",
                                "id": "p02qsqf5",
                                "hd": false,
                                "download": false,
                                "availability": {
                                    "start": "2015-05-19T15:00:00Z",
                                    "end": "2015-06-18T15:00:00Z",
                                    "remaining": {
                                        "text": "Available for 10 days"
                                    }
                                },
                                "kind": "original",
                                "duration": {
                                    "value": "PT47M47S",
                                    "text": "48 mins"
                                },
                                "first_broadcast": "24 Nov 2010"
                            }
                        ],
                        "status": "available"
                    }
                ],
                "tleo_type": "series",
                "type": "programme_large",
                "title": "Cartrefi Cefn Gwlad Cymru",
                "synopses": {
                    "small": "Homes and architecture in the Welsh countryside. Homes and architecture in the Welsh co...",
                    "medium": "Homes and architecture in the Welsh countryside. Homes and architecture in the Welsh countryside.",
                    "large": "Homes and architecture in the Welsh countryside. Homes and architecture in the Welsh countryside."
                },
                "status": "available",
                "images": {
                    "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p02gbsqq.jpg",
                    "type": "image"
                },
                "lexical_sort_letter": "C",
                "master_brand": {
                    "id": "s4cpbs",
                    "attribution": "s4c",
                    "titles": {
                        "small": "S4C",
                        "medium": "S4C",
                        "large": "S4C"
                    }
                },
                "count": 2
            },
            {
                "id": "b006v7ww",
                "initial_children": [
                    {
                        "id": "b00z8f6w",
                        "type": "episode",
                        "title": "Cash in the Attic",
                        "lexical_sort_letter": "C",
                        "synopses": {
                            "small": "Jennie Bond and John Cameron help Martin, who wishes to fund a memorial to his uncle.",
                            "medium": "Antiques series. Martin has inherited heirlooms from his uncle, and wishes to sell them to fund a memorial to him in the garden. Jennie Bond and John Cameron are on hand to help.",
                            "large": "Martin Quilter has inherited heirlooms from his uncle Alex, and would like to sell them in order to create a memorial to him in the garden. He is joined by his fiancee Mary, Jennie Bond and John Cameron to look through the collectibles around their home in Leicestershire."
                        },
                        "tleo_id": "b006v7ww",
                        "tleo_type": "brand",
                        "images": {
                            "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p01lcb7y.jpg",
                            "type": "image",
                            "inherited_from": "brand"
                        },
                        "guidance": false,
                        "related_links": [],
                        "subtitle": "Series 17: 57. Quilter",
                        "release_date": "1 Mar 2011",
                        "parent_id": "b00rzq3h",
                        "master_brand": {
                            "id": "bbc_one",
                            "ident_id": "p00pvbj7",
                            "attribution": "bbc_one",
                            "titles": {
                                "small": "BBC One",
                                "medium": "BBC One",
                                "large": "BBC One"
                            }
                        },
                        "versions": [
                            {
                                "type": "version",
                                "id": "b05y5xr9",
                                "hd": false,
                                "download": true,
                                "availability": {
                                    "start": "2015-06-04T13:45:00Z",
                                    "end": "2015-07-04T13:45:00Z",
                                    "remaining": {
                                        "text": "Available for 26 days"
                                    }
                                },
                                "kind": "editorial",
                                "duration": {
                                    "value": "PT27M",
                                    "text": "27 mins"
                                },
                                "first_broadcast": "1 Mar 2011"
                            },
                            {
                                "type": "version",
                                "id": "b05y5xr9",
                                "hd": false,
                                "download": true,
                                "availability": {
                                    "start": "2015-06-04T13:45:00Z",
                                    "end": "2015-07-04T13:45:00Z",
                                    "remaining": {
                                        "text": "Available for 26 days"
                                    }
                                },
                                "kind": "shortened",
                                "duration": {
                                    "value": "PT27M",
                                    "text": "27 mins"
                                },
                                "first_broadcast": "1 Mar 2011"
                            }
                        ],
                        "status": "available"
                    }
                ],
                "tleo_type": "brand",
                "type": "programme_large",
                "title": "Cash in the Attic",
                "synopses": {
                    "small": "The show that turns hidden treasures into cash and viewers' dreams into reality",
                    "medium": "The show that turns hidden treasures into cash and viewers' dreams into reality."
                },
                "status": "available",
                "images": {
                    "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p01lcb7y.jpg",
                    "type": "image"
                },
                "lexical_sort_letter": "C",
                "master_brand": {
                    "id": "bbc_one",
                    "ident_id": "p00pvbj7",
                    "attribution": "bbc_one",
                    "titles": {
                        "small": "BBC One",
                        "medium": "BBC One",
                        "large": "BBC One"
                    }
                },
                "count": 12
            },
            {
                "id": "b04t9h6l",
                "initial_children": [
                    {
                        "id": "b04tt2f9",
                        "type": "episode",
                        "title": "Castles: Britain's Fortified History",
                        "lexical_sort_letter": "C",
                        "synopses": {
                            "small": "The story of Edward I, who turned the castle into an instrument of colonisation.",
                            "medium": "Sam Willis tells the story of the English ruler who left the most indelible mark on the castle, Edward I, as he turned it into an instrument of colonisation.",
                            "large": "Sam Willis tells the story of the English ruler who left the most indelible mark on the castle - the great Plantagenet king, Edward I, who turned it into an instrument of colonisation. Edward spent vast sums to subdue Wales with a ring of iron comprised of some of the most fearsome fortresses ever built. Castles like Caernarfon and Beaumaris were used to impose England's will on the Welsh. But when Edward turned his attention to Scotland, laying siege to castles with great catapults, things didn't go so well for him."
                        },
                        "tleo_id": "b04t9h6l",
                        "tleo_type": "series",
                        "images": {
                            "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p02dcyrg.jpg",
                            "type": "image"
                        },
                        "guidance": false,
                        "related_links": [],
                        "subtitle": "2. Kingdom of Conquest",
                        "release_date": "11 Dec 2014",
                        "parent_id": "b04t9h6l",
                        "master_brand": {
                            "id": "bbc_four",
                            "ident_id": "p00pvbh0",
                            "attribution": "bbc_four",
                            "titles": {
                                "small": "BBC Four",
                                "medium": "BBC Four",
                                "large": "BBC Four"
                            }
                        },
                        "versions": [
                            {
                                "type": "version",
                                "id": "b04tt280",
                                "hd": true,
                                "download": true,
                                "availability": {
                                    "start": "2015-06-04T20:00:00Z",
                                    "end": "2015-07-04T20:00:00Z",
                                    "remaining": {
                                        "text": "Available for 26 days"
                                    }
                                },
                                "kind": "original",
                                "duration": {
                                    "value": "PT1H",
                                    "text": "60 mins"
                                },
                                "first_broadcast": "11 Dec 2014"
                            },
                            {
                                "type": "version",
                                "id": "p02f2mp7",
                                "hd": false,
                                "download": false,
                                "availability": {
                                    "start": "2015-06-05T10:31:54Z",
                                    "end": "2015-07-04T20:00:00Z",
                                    "remaining": {
                                        "text": "Available for 26 days"
                                    }
                                },
                                "kind": "audio-described",
                                "duration": {
                                    "value": "PT1H",
                                    "text": "60 mins"
                                },
                                "first_broadcast": "11 Dec 2014"
                            },
                            {
                                "type": "version",
                                "id": "b04v226q",
                                "hd": false,
                                "download": true,
                                "availability": {
                                    "start": "2015-06-05T02:05:00Z",
                                    "end": "2015-07-05T02:05:00Z",
                                    "remaining": {
                                        "text": "Available for 26 days"
                                    }
                                },
                                "kind": "signed",
                                "duration": {
                                    "value": "PT1H",
                                    "text": "60 mins"
                                },
                                "first_broadcast": "11 Dec 2014"
                            }
                        ],
                        "status": "available"
                    }
                ],
                "tleo_type": "series",
                "type": "programme_large",
                "title": "Castles: Britain's Fortified History",
                "synopses": {
                    "small": "Sam Willis traces the story of Britain's castles in our history, art and literature.",
                    "medium": "Historian Sam Willis traces the story of Britain's castles and their unique role in our history, art and literature."
                },
                "status": "available",
                "images": {
                    "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p02ck2fb.jpg",
                    "type": "image"
                },
                "lexical_sort_letter": "C",
                "master_brand": {
                    "id": "bbc_four",
                    "ident_id": "p00pvbh0",
                    "attribution": "bbc_four",
                    "titles": {
                        "small": "BBC Four",
                        "medium": "BBC Four",
                        "large": "BBC Four"
                    }
                },
                "count": 2
            },
            {
                "id": "b006m8wd",
                "initial_children": [
                    {
                        "id": "b05xv4n4",
                        "type": "episode",
                        "title": "Casualty",
                        "lexical_sort_letter": "C",
                        "synopses": {
                            "small": "Cal and Ethan help a son come to terms with the seriousness of his mother's illness.",
                            "medium": "Cal and Ethan help a son come to terms with the seriousness of his mother's illness, while Zoe and Dylan unite a family."
                        },
                        "tleo_id": "b006m8wd",
                        "tleo_type": "brand",
                        "images": {
                            "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p02sg0x4.jpg",
                            "type": "image"
                        },
                        "guidance": false,
                        "related_links": [],
                        "subtitle": "Series 29: 34. Fix You",
                        "release_date": "6 Jun 2015",
                        "parent_id": "b04gr79t",
                        "master_brand": {
                            "id": "bbc_one",
                            "ident_id": "p00pvbj7",
                            "attribution": "bbc_one",
                            "titles": {
                                "small": "BBC One",
                                "medium": "BBC One",
                                "large": "BBC One"
                            }
                        },
                        "versions": [
                            {
                                "type": "version",
                                "id": "b05xv4hs",
                                "hd": true,
                                "download": true,
                                "availability": {
                                    "start": "2015-06-06T20:45:00Z",
                                    "end": "2015-07-06T20:45:00Z",
                                    "remaining": {
                                        "text": "Available for 28 days"
                                    }
                                },
                                "kind": "original",
                                "duration": {
                                    "value": "PT50M",
                                    "text": "50 mins"
                                },
                                "first_broadcast": "6 Jun 2015"
                            },
                            {
                                "type": "version",
                                "id": "p02t3dl9",
                                "hd": false,
                                "download": false,
                                "availability": {
                                    "start": "2015-06-06T21:08:36Z",
                                    "end": "2015-07-06T20:45:00Z",
                                    "remaining": {
                                        "text": "Available for 28 days"
                                    }
                                },
                                "kind": "audio-described",
                                "duration": {
                                    "value": "PT50M",
                                    "text": "50 mins"
                                },
                                "first_broadcast": "6 Jun 2015"
                            }
                        ],
                        "status": "available"
                    }
                ],
                "tleo_type": "brand",
                "type": "programme_large",
                "title": "Casualty",
                "synopses": {
                    "small": "Drama series about the staff and patients at Holby City Hospital's emergency department.",
                    "medium": "Drama series about the staff and patients at Holby City Hospital's emergency department, charting the ups and downs in their personal and professional lives."
                },
                "status": "available",
                "images": {
                    "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p01t1v7g.jpg",
                    "type": "image"
                },
                "lexical_sort_letter": "C",
                "master_brand": {
                    "id": "bbc_one",
                    "ident_id": "p00pvbj7",
                    "attribution": "bbc_one",
                    "titles": {
                        "small": "BBC One",
                        "medium": "BBC One",
                        "large": "BBC One"
                    }
                },
                "count": 3
            },
            {
                "id": "b05vt8cm",
                "initial_children": [
                    {
                        "id": "b05yl3kr",
                        "type": "episode",
                        "title": "CBBC Official Chart Show",
                        "lexical_sort_letter": "C",
                        "synopses": {
                            "small": "Cel Spellman counts down the weekly official chart from BBC Radio 1's studios.",
                            "medium": "Live from BBC Radio 1's world-famous studios, Cel Spellman counts down the weekly official chart. Featuring special guests.",
                            "large": "Cel Spellman counts down the weekly official chart from BBC Radio 1's world-famous studios. Featuring special guests, flashmobs, lipdubs, viewer's videos, and the all-important number one."
                        },
                        "tleo_id": "b05vt8cm",
                        "tleo_type": "series",
                        "images": {
                            "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p02t67zx.jpg",
                            "type": "image"
                        },
                        "guidance": false,
                        "related_links": [],
                        "subtitle": "5. Chart Show with Oritsé",
                        "release_date": "7 Jun 2015",
                        "parent_id": "b05vt8cm",
                        "master_brand": {
                            "id": "cbbc",
                            "ident_id": "p00pvbn6",
                            "attribution": "cbbc",
                            "titles": {
                                "small": "CBBC",
                                "medium": "CBBC",
                                "large": "CBBC"
                            }
                        },
                        "versions": [
                            {
                                "type": "version",
                                "id": "b05yl3kf",
                                "hd": true,
                                "download": true,
                                "availability": {
                                    "start": "2015-06-07T18:00:00Z",
                                    "end": "2015-07-07T18:00:00Z",
                                    "remaining": {
                                        "text": "Available for 29 days"
                                    }
                                },
                                "kind": "original",
                                "duration": {
                                    "value": "PT28M",
                                    "text": "28 mins"
                                },
                                "first_broadcast": "6:30pm 7 Jun 2015"
                            }
                        ],
                        "status": "available"
                    }
                ],
                "tleo_type": "series",
                "type": "programme_large",
                "title": "CBBC Official Chart Show",
                "synopses": {
                    "small": "Live from BBC Radio 1's studios, Cel Spellman counts down the weekly official chart.",
                    "medium": "Live from BBC Radio 1's world-famous studios, Cel Spellman counts down the weekly official chart, featuring special guests and more."
                },
                "status": "available",
                "images": {
                    "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p02qscrc.jpg",
                    "type": "image"
                },
                "lexical_sort_letter": "C",
                "master_brand": {
                    "id": "cbbc",
                    "ident_id": "p00pvbn6",
                    "attribution": "cbbc",
                    "titles": {
                        "small": "CBBC",
                        "medium": "CBBC",
                        "large": "CBBC"
                    }
                },
                "count": 5
            },
            {
                "id": "b00jdlm2",
                "initial_children": [
                    {
                        "id": "b05yxp03",
                        "type": "episode",
                        "title": "CBeebies Bedtime Stories",
                        "lexical_sort_letter": "C",
                        "synopses": {
                            "small": "Run, run as fast as you can. Derek Jacobi reads Gingerbread Man.",
                            "medium": "An exciting bedtime story. Run, run as fast as you can. You can't catch me, I'm the gingerbread man! Derek Jacobi reads Gingerbread Man.",
                            "large": "Run, run as fast as you can. You can't catch me, I'm the gingerbread man! Everyone wants to catch him but he's just too fast. Sir Derek Jacobi reads a version of this classic tale."
                        },
                        "tleo_id": "b00jdlm2",
                        "tleo_type": "series",
                        "images": {
                            "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p02sx7rn.jpg",
                            "type": "image"
                        },
                        "guidance": false,
                        "related_links": [],
                        "subtitle": "498. Derek Jacobi - Gingerbread Man",
                        "release_date": "7 Jun 2015",
                        "parent_id": "b00jdlm2",
                        "master_brand": {
                            "id": "cbeebies",
                            "ident_id": "p00pvbng",
                            "attribution": "cbeebies",
                            "titles": {
                                "small": "CBeebies",
                                "medium": "CBeebies",
                                "large": "CBeebies"
                            }
                        },
                        "versions": [
                            {
                                "type": "version",
                                "id": "b05yxp01",
                                "hd": true,
                                "download": true,
                                "availability": {
                                    "start": "2015-06-07T18:00:00Z",
                                    "end": "2015-07-07T18:00:00Z",
                                    "remaining": {
                                        "text": "Available for 29 days"
                                    }
                                },
                                "kind": "original",
                                "duration": {
                                    "value": "PT5M",
                                    "text": "5 mins"
                                },
                                "first_broadcast": "6:50pm 7 Jun 2015"
                            }
                        ],
                        "status": "available"
                    }
                ],
                "tleo_type": "series",
                "type": "programme_large",
                "title": "CBeebies Bedtime Stories",
                "synopses": {
                    "small": "A different story read each night just before bedtime",
                    "medium": "A different story read each night just before bedtime"
                },
                "status": "available",
                "images": {
                    "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p01w5205.jpg",
                    "type": "image"
                },
                "lexical_sort_letter": "C",
                "master_brand": {
                    "id": "cbeebies",
                    "ident_id": "p00pvbng",
                    "attribution": "cbeebies",
                    "titles": {
                        "small": "CBeebies",
                        "medium": "CBeebies",
                        "large": "CBeebies"
                    }
                },
                "count": 30
            },
            {
                "id": "p02b4l6s",
                "initial_children": [
                    {
                        "id": "p02rv1z4",
                        "type": "episode",
                        "title": "Cefn Gwlad",
                        "lexical_sort_letter": "C",
                        "synopses": {
                            "small": "Dai Jones, Llanilar sy'n ymweld â Meilir Jones a'r teulu, ar Fferm y Gop, Trelawnyd, Si...",
                            "medium": "Dai Jones, Llanilar sy'n ymweld â Meilir Jones a'r teulu, ar Fferm y Gop, Trelawnyd, Sir y Fflint. Dai Jones visits Meilir Jones and his family at Gop Farm, Trelawnyd, Flintshire.",
                            "large": "Dai Jones, Llanilar sy'n ymweld â Meilir Jones a'r teulu, ar Fferm y Gop, Trelawnyd, Sir y Fflint. Dai Jones visits Meilir Jones and his family at Gop Farm, Trelawnyd, Flintshire."
                        },
                        "tleo_id": "p02b4l6s",
                        "tleo_type": "brand",
                        "images": {
                            "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p02sh2d8.jpg",
                            "type": "image"
                        },
                        "guidance": false,
                        "related_links": [],
                        "subtitle": "Cyfres 2014: Cefn Gwlad, Teulu'r Gop",
                        "release_date": "1 Jun 2015",
                        "parent_id": "p02cjx1f",
                        "master_brand": {
                            "id": "s4cpbs",
                            "attribution": "s4c",
                            "titles": {
                                "small": "S4C",
                                "medium": "S4C",
                                "large": "S4C"
                            }
                        },
                        "versions": [
                            {
                                "type": "version",
                                "id": "p02s9p7j",
                                "hd": false,
                                "download": true,
                                "availability": {
                                    "start": "2015-06-05T16:26:20Z",
                                    "end": "2015-07-01T20:00:00Z",
                                    "remaining": {
                                        "text": "Available for 23 days"
                                    }
                                },
                                "kind": "signed",
                                "duration": {
                                    "value": "PT23M30S",
                                    "text": "24 mins"
                                },
                                "first_broadcast": "1 Jun 2015"
                            }
                        ],
                        "status": "available"
                    }
                ],
                "tleo_type": "brand",
                "type": "programme_large",
                "title": "Cefn Gwlad",
                "synopses": {
                    "small": "Crwydro cefn gwlad Cymru yng nghwmni Dai Jones. Dai Jones visits people and businesses ...",
                    "medium": "Crwydro cefn gwlad Cymru yng nghwmni Dai Jones. Dai Jones visits people and businesses in the Welsh countryside.",
                    "large": "Crwydro cefn gwlad Cymru yng nghwmni Dai Jones. Dai Jones visits people and businesses in the Welsh countryside."
                },
                "status": "available",
                "images": {
                    "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p02gbshd.jpg",
                    "type": "image"
                },
                "lexical_sort_letter": "C",
                "master_brand": {
                    "id": "s4cpbs",
                    "attribution": "s4c",
                    "titles": {
                        "small": "S4C",
                        "medium": "S4C",
                        "large": "S4C"
                    }
                },
                "count": 3
            },
            {
                "id": "p02kt55v",
                "initial_children": [
                    {
                        "id": "p02qycwp",
                        "type": "episode",
                        "title": "Cei Bach",
                        "lexical_sort_letter": "C",
                        "synopses": {
                            "small": "Mae gan bawb ei freuddwyd, a breuddwyd arbennig Huwi Stomp ydy cael beic. Huwi Stomp ha...",
                            "medium": "Mae gan bawb ei freuddwyd, a breuddwyd arbennig Huwi Stomp ydy cael beic. Huwi Stomp has always dreamed of owning his own bike but when he gets one he doesn't use it. I wonder why?",
                            "large": "Mae gan bawb ei freuddwyd, a breuddwyd arbennig Huwi Stomp ydy cael beic. Huwi Stomp has always dreamed of owning his own bike but when he gets one he doesn't use it. I wonder why?"
                        },
                        "tleo_id": "p02kt55v",
                        "tleo_type": "series",
                        "images": {
                            "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p02kw176.jpg",
                            "type": "image",
                            "inherited_from": "series"
                        },
                        "guidance": false,
                        "related_links": [],
                        "subtitle": "Huwi'n Cael Beic",
                        "release_date": "15 Feb 2012",
                        "parent_id": "p02kt55v",
                        "master_brand": {
                            "id": "s4cpbs",
                            "attribution": "s4c",
                            "titles": {
                                "small": "S4C",
                                "medium": "S4C",
                                "large": "S4C"
                            }
                        },
                        "versions": [
                            {
                                "type": "version",
                                "id": "p02rc790",
                                "hd": false,
                                "download": true,
                                "availability": {
                                    "start": "2015-05-21T09:50:00Z",
                                    "end": "2015-06-20T09:50:00Z",
                                    "remaining": {
                                        "text": "Available for 11 days"
                                    }
                                },
                                "kind": "technical-replacement",
                                "duration": {
                                    "value": "PT14M29S",
                                    "text": "14 mins"
                                },
                                "first_broadcast": "15 Feb 2012"
                            },
                            {
                                "type": "version",
                                "id": "p02rc790",
                                "hd": false,
                                "download": true,
                                "availability": {
                                    "start": "2015-05-21T09:50:00Z",
                                    "end": "2015-06-20T09:50:00Z",
                                    "remaining": {
                                        "text": "Available for 11 days"
                                    }
                                },
                                "kind": "original",
                                "duration": {
                                    "value": "PT14M29S",
                                    "text": "14 mins"
                                },
                                "first_broadcast": "15 Feb 2012"
                            }
                        ],
                        "status": "available"
                    }
                ],
                "tleo_type": "series",
                "type": "programme_large",
                "title": "Cei Bach",
                "synopses": {
                    "small": "Cyfres boblogaidd i blant meithrin yn dilyn hynt a helynt trigolion lliwgar Cei Bach. P...",
                    "medium": "Cyfres boblogaidd i blant meithrin yn dilyn hynt a helynt trigolion lliwgar Cei Bach. Popular pre-school series following the colourful adventures of the residents of Cei Bach.",
                    "large": "Cyfres boblogaidd i blant meithrin yn dilyn hynt a helynt trigolion lliwgar Cei Bach. Popular pre-school series following the colourful adventures of the residents of Cei Bach."
                },
                "status": "available",
                "images": {
                    "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p02kw176.jpg",
                    "type": "image"
                },
                "lexical_sort_letter": "C",
                "master_brand": {
                    "id": "s4cpbs",
                    "attribution": "s4c",
                    "titles": {
                        "small": "S4C",
                        "medium": "S4C",
                        "large": "S4C"
                    }
                },
                "count": 2
            },
            {
                "id": "p018vrwm",
                "initial_children": [
                    {
                        "id": "b03bs8x8",
                        "type": "episode",
                        "title": "Ceistean Lara",
                        "lexical_sort_letter": "C",
                        "synopses": {
                            "small": "Monica moves in with Akira, but their close quarters leads to an argument between them.",
                            "medium": "Children's animated series. Monica moves in with Akira, but their close quarters leads to an argument between them, leaving Lara feeling confused and left out.",
                            "large": "Tha Monica a' gluasad a-steach còmhla ri Akira ach chan fhada gus a bheil argamaid eadar an dithis, a' fàgail Lara a' faireachdainn troimhe-a-chèile agus a-mach à cùisean. Aig a' cheart àm tha Tònaidh a' cur teacsa le dealbh gu an caraidean uile, a' fàgail Gabriel air a nàrachadh.\n\nMonica moves in with Akira, but their close quarters leads to an argument between them, leaving Lara feeling confused and left out. Meanwhile, Tony sends a picture text to all their friends which embarrasses Gabriel."
                        },
                        "tleo_id": "p018vrwm",
                        "tleo_type": "series",
                        "images": {
                            "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p01f4w00.jpg",
                            "type": "image"
                        },
                        "guidance": false,
                        "related_links": [],
                        "subtitle": "9. Diofar Dhòighean",
                        "release_date": "4 Oct 2013",
                        "parent_id": "p018vrwm",
                        "master_brand": {
                            "id": "bbc_alba",
                            "ident_id": "p00pvbgm",
                            "attribution": "bbc_alba",
                            "titles": {
                                "small": "BBC ALBA",
                                "medium": "BBC ALBA",
                                "large": "BBC ALBA"
                            }
                        },
                        "versions": [
                            {
                                "type": "version",
                                "id": "b03bs8x6",
                                "hd": false,
                                "download": true,
                                "availability": {
                                    "start": "2015-06-02T17:36:44Z",
                                    "end": "2015-07-04T17:15:00Z",
                                    "remaining": {
                                        "text": "Available for 26 days"
                                    }
                                },
                                "kind": "original",
                                "duration": {
                                    "value": "PT12M",
                                    "text": "12 mins"
                                },
                                "first_broadcast": "6:50pm 4 Oct 2013"
                            }
                        ],
                        "status": "available"
                    }
                ],
                "tleo_type": "series",
                "type": "programme_large",
                "title": "Ceistean Lara",
                "synopses": {
                    "small": "Lean Lara 's a caraidean 's iad a' dèiligeadh ri diofar rudan a tha tachairt nam beatha.",
                    "medium": "Lean Lara 's a caraidean 's iad a' dèiligeadh ri diofar rudan a tha tachairt nam beatha."
                },
                "status": "available",
                "images": {
                    "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p01fn8l4.jpg",
                    "type": "image"
                },
                "lexical_sort_letter": "C",
                "master_brand": {
                    "id": "bbc_alba",
                    "ident_id": "p00pvbgm",
                    "attribution": "bbc_alba",
                    "titles": {
                        "small": "BBC ALBA",
                        "medium": "BBC ALBA",
                        "large": "BBC ALBA"
                    }
                },
                "count": 4
            },
            {
                "id": "b008pc8h",
                "initial_children": [
                    {
                        "id": "b01py5jp",
                        "type": "episode",
                        "title": "Celebrity Mastermind",
                        "lexical_sort_letter": "C",
                        "synopses": {
                            "small": "John Humphrys quizzes Cheryl Baker, Guy Henry, Mark Thompson and Tim Bentinck.",
                            "medium": "Celebrity quiz. Bucks Fizz star Cheryl Baker, Holby City actor Guy Henry, Stargazing Live astronomer Mark Thompson and actor Tim Bentinck answer questions from John Humphrys.",
                            "large": "Bucks Fizz star Cheryl Baker, Holby City actor Guy Henry, Stargazing Live astronomer Mark Thompson and Archers actor Tim Bentinck face the questions of John Humphrys for the last in the current series. The specialist subjects include the films of Peter O'Toole, AA Milne and Winnie the Pooh, the music of James Taylor and the history of coffee."
                        },
                        "tleo_id": "b008pc8h",
                        "tleo_type": "brand",
                        "images": {
                            "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p01lcd0m.jpg",
                            "type": "image",
                            "inherited_from": "brand"
                        },
                        "guidance": false,
                        "related_links": [],
                        "subtitle": "2012/2013: Episode 10",
                        "release_date": "5 Jan 2013",
                        "parent_id": "b01pqf33",
                        "master_brand": {
                            "id": "bbc_one",
                            "ident_id": "p00pvbj7",
                            "attribution": "bbc_one",
                            "titles": {
                                "small": "BBC One",
                                "medium": "BBC One",
                                "large": "BBC One"
                            }
                        },
                        "versions": [
                            {
                                "type": "version",
                                "id": "b01py5jj",
                                "hd": true,
                                "download": true,
                                "availability": {
                                    "start": "2015-05-16T16:50:00Z",
                                    "end": "2015-06-15T16:50:00Z",
                                    "remaining": {
                                        "text": "Available for 7 days"
                                    }
                                },
                                "kind": "original",
                                "duration": {
                                    "value": "PT30M",
                                    "text": "30 mins"
                                },
                                "first_broadcast": "5 Jan 2013"
                            }
                        ],
                        "status": "available"
                    }
                ],
                "tleo_type": "brand",
                "type": "programme_large",
                "title": "Celebrity Mastermind",
                "synopses": {
                    "small": "John Humphrys presents a celebrity version of Britain's toughest quiz.",
                    "medium": "John Humphrys presents a celebrity version of Britain's toughest quiz."
                },
                "status": "available",
                "images": {
                    "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p01lcd0m.jpg",
                    "type": "image"
                },
                "lexical_sort_letter": "C",
                "master_brand": {
                    "id": "bbc_one",
                    "ident_id": "p00pvbj7",
                    "attribution": "bbc_one",
                    "titles": {
                        "small": "BBC One",
                        "medium": "BBC One",
                        "large": "BBC One"
                    }
                },
                "count": 2
            },
            {
                "id": "p014pcxx",
                "initial_children": [
                    {
                        "id": "p014g298",
                        "type": "episode",
                        "title": "Celebrity Recital",
                        "lexical_sort_letter": "C",
                        "synopses": {
                            "small": "A concert given to an audience of friends by Benjamin Britten and Peter Pears. (1964)",
                            "medium": "First transmitted in 1964,  this is an informal concert given to an audience of friends at the Riverside Studios, London, by Benjamin Britten and Peter Pears.",
                            "large": "First transmitted in 1964,  this is an informal concert of traditional English songs given to an audience of friends at the Riverside Studios, London, by Benjamin Britten and Peter Pears."
                        },
                        "tleo_id": "p014pcxx",
                        "tleo_type": "brand",
                        "images": {
                            "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p014pf2t.jpg",
                            "type": "image"
                        },
                        "guidance": false,
                        "related_links": [
                            {
                                "id": "p0269rtk",
                                "type": "link",
                                "kind": "priority_content",
                                "title": "More Modern Classical Music",
                                "url": "http://www.bbc.co.uk/iplayer/group/p014vhd3"
                            }
                        ],
                        "subtitle": "Benjamin Britten and Peter Pears",
                        "release_date": "21 Jun 1964",
                        "parent_id": "p014pcxx",
                        "master_brand": {
                            "id": "bbc_webonly",
                            "attribution": "bbc",
                            "titles": {
                                "small": "BBC",
                                "medium": "BBC",
                                "large": "BBC"
                            }
                        },
                        "versions": [
                            {
                                "type": "version",
                                "id": "p02hhmj1",
                                "hd": false,
                                "download": false,
                                "availability": {
                                    "start": "2015-01-23T18:59:30Z",
                                    "remaining": {
                                        "text": "Available for over a year"
                                    }
                                },
                                "kind": "original",
                                "duration": {
                                    "value": "PT27M",
                                    "text": "27 mins"
                                },
                                "first_broadcast": "21 Jun 1964"
                            }
                        ],
                        "status": "available",
                        "labels": {
                            "editorial": "Archive"
                        }
                    }
                ],
                "tleo_type": "brand",
                "type": "programme_large",
                "title": "Celebrity Recital",
                "synopses": {
                    "small": "Concerts and musical performances",
                    "medium": "Concerts and musical performances",
                    "large": "Concerts and musical performances"
                },
                "status": "available",
                "images": {
                    "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p014pf0m.jpg",
                    "type": "image"
                },
                "lexical_sort_letter": "C",
                "master_brand": {
                    "id": "bbc_webonly",
                    "attribution": "bbc",
                    "titles": {
                        "small": "BBC",
                        "medium": "BBC",
                        "large": "BBC"
                    }
                },
                "count": 1
            },
            {
                "id": "p02gtrqd",
                "initial_children": [
                    {
                        "id": "p02kw4pn",
                        "type": "episode",
                        "title": "Celwydd Noeth",
                        "lexical_sort_letter": "C",
                        "synopses": {
                            "small": "Yn mynd amdani yn rhaglen ola'r gyfres mae'r ffrindiau bore oes, Colin ac Andrew o Beny...",
                            "medium": "Yn mynd amdani yn rhaglen ola'r gyfres mae'r ffrindiau bore oes, Colin ac Andrew o Benygroes. Competing tonight are lifelong friends Colin and Andrew from Penygroes near Caernar...",
                            "large": "Yn mynd amdani yn rhaglen ola'r gyfres mae'r ffrindiau bore oes, Colin ac Andrew o Benygroes. Competing tonight are lifelong friends Colin and Andrew from Penygroes near Caernarfon."
                        },
                        "tleo_id": "p02gtrqd",
                        "tleo_type": "series",
                        "images": {
                            "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p02lphkm.jpg",
                            "type": "image"
                        },
                        "guidance": false,
                        "related_links": [],
                        "subtitle": "Pennod 8",
                        "release_date": "12 Mar 2015",
                        "parent_id": "p02gtrqd",
                        "master_brand": {
                            "id": "s4cpbs",
                            "attribution": "s4c",
                            "titles": {
                                "small": "S4C",
                                "medium": "S4C",
                                "large": "S4C"
                            }
                        },
                        "versions": [
                            {
                                "type": "version",
                                "id": "p02kw4ps",
                                "hd": false,
                                "download": false,
                                "availability": {
                                    "start": "2015-05-13T18:00:00Z",
                                    "end": "2015-06-12T18:00:00Z",
                                    "remaining": {
                                        "text": "Available until Fri 7pm"
                                    }
                                },
                                "kind": "original",
                                "duration": {
                                    "value": "PT23M48S",
                                    "text": "24 mins"
                                },
                                "first_broadcast": "12 Mar 2015"
                            }
                        ],
                        "status": "available"
                    }
                ],
                "tleo_type": "series",
                "type": "programme_large",
                "title": "Celwydd Noeth",
                "synopses": {
                    "small": "Cwis go wahanol! Does yna ddim cwestiynau! Yr her fydd dewis pa gelwyddau noeth sy'n cu...",
                    "medium": "Cwis go wahanol! Does yna ddim cwestiynau! Yr her fydd dewis pa gelwyddau noeth sy'n cuddio ynghanol cyfres o ffeithiau. A quiz with a twist - there are no questions! Nia Robert...",
                    "large": "Cwis go wahanol! Does yna ddim cwestiynau! Yr her fydd dewis pa gelwyddau noeth sy'n cuddio ynghanol cyfres o ffeithiau. A quiz with a twist - there are no questions! Nia Roberts presents."
                },
                "status": "available",
                "images": {
                    "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p02h7xc6.jpg",
                    "type": "image"
                },
                "lexical_sort_letter": "C",
                "master_brand": {
                    "id": "s4cpbs",
                    "attribution": "s4c",
                    "titles": {
                        "small": "S4C",
                        "medium": "S4C",
                        "large": "S4C"
                    }
                },
                "count": 1
            },
            {
                "id": "b05xgg9m",
                "initial_children": [
                    {
                        "id": "b05xgg9m",
                        "type": "episode",
                        "title": "Ceòl air Abhainn Chluaidh",
                        "lexical_sort_letter": "C",
                        "synopses": {
                            "small": "Linda NicLeòid le ceòl tradiseanta. Linda MacLeod presents traditional music.",
                            "medium": "To mark BBC Music Day, Linda MacLeod is onstage presenting a unique hour-long in-concert special that represents the best of Scottish and Gaelic traditional music.",
                            "large": "A' comharrachadh là-ciùil a' BhBC, tha tè-an-taighe Linda NicLeòid air an àrd-ùrlar, a' libhrigeadh cuirm sònraichte; uair a thìde de cheòl a tha a' riochdachadh ceòl traidiseanta Albannach is Gàidhealach. Tha Ceòl air a Chluaidh a' toirt cuirmean agus seataichean mhìorbhaileach dhuinn a tha a' foillseachadh an neart agus an smior a tha air cùl saoghal a' chiùil ann an Alba.\n\nTo mark BBC Music Day, our host Linda MacLeod is onstage presenting a unique hour-long in-concert special that represents the best of Scottish and Gaelic traditional music. Ceòl air a' Chluaidh brings us marvellous musical sets that prove the strength and quality of the Scottish music world."
                        },
                        "tleo_id": "b05xgg9m",
                        "tleo_type": "episode",
                        "images": {
                            "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p02sx0h1.jpg",
                            "type": "image"
                        },
                        "guidance": false,
                        "related_links": [],
                        "release_date": "5 Jun 2015",
                        "master_brand": {
                            "id": "bbc_alba",
                            "ident_id": "p00pvbgm",
                            "attribution": "bbc_alba",
                            "titles": {
                                "small": "BBC ALBA",
                                "medium": "BBC ALBA",
                                "large": "BBC ALBA"
                            }
                        },
                        "versions": [
                            {
                                "type": "version",
                                "id": "b05xgg1v",
                                "hd": false,
                                "download": true,
                                "availability": {
                                    "start": "2015-06-05T22:22:45Z",
                                    "end": "2015-07-05T21:00:00Z",
                                    "remaining": {
                                        "text": "Available for 27 days"
                                    }
                                },
                                "kind": "original",
                                "duration": {
                                    "value": "PT59M",
                                    "text": "59 mins"
                                },
                                "first_broadcast": "5 Jun 2015"
                            },
                            {
                                "type": "version",
                                "id": "b05xgg1v",
                                "hd": false,
                                "download": true,
                                "availability": {
                                    "start": "2015-06-05T22:22:45Z",
                                    "end": "2015-07-05T21:00:00Z",
                                    "remaining": {
                                        "text": "Available for 27 days"
                                    }
                                },
                                "kind": "open-subtitled",
                                "duration": {
                                    "value": "PT59M",
                                    "text": "59 mins"
                                },
                                "first_broadcast": "5 Jun 2015"
                            }
                        ],
                        "status": "available"
                    }
                ],
                "tleo_type": "episode",
                "type": "programme_large",
                "title": "Ceòl air Abhainn Chluaidh",
                "synopses": {
                    "small": "Linda NicLeòid le ceòl tradiseanta. Linda MacLeod presents traditional music.",
                    "medium": "To mark BBC Music Day, Linda MacLeod is onstage presenting a unique hour-long in-concert special that represents the best of Scottish and Gaelic traditional music.",
                    "large": "A' comharrachadh là-ciùil a' BhBC, tha tè-an-taighe Linda NicLeòid air an àrd-ùrlar, a' libhrigeadh cuirm sònraichte; uair a thìde de cheòl a tha a' riochdachadh ceòl traidiseanta Albannach is Gàidhealach. Tha Ceòl air a Chluaidh a' toirt cuirmean agus seataichean mhìorbhaileach dhuinn a tha a' foillseachadh an neart agus an smior a tha air cùl saoghal a' chiùil ann an Alba.\n\nTo mark BBC Music Day, our host Linda MacLeod is onstage presenting a unique hour-long in-concert special that represents the best of Scottish and Gaelic traditional music. Ceòl air a' Chluaidh brings us marvellous musical sets that prove the strength and quality of the Scottish music world."
                },
                "status": "available",
                "images": {
                    "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p02sx0h1.jpg",
                    "type": "image"
                },
                "lexical_sort_letter": "C",
                "master_brand": {
                    "id": "bbc_alba",
                    "ident_id": "p00pvbgm",
                    "attribution": "bbc_alba",
                    "titles": {
                        "small": "BBC ALBA",
                        "medium": "BBC ALBA",
                        "large": "BBC ALBA"
                    }
                },
                "count": 1
            },
            {
                "id": "b04hzg2p",
                "initial_children": [
                    {
                        "id": "b0511vbd",
                        "type": "episode",
                        "title": "Ceòl bho Perthshire Amber",
                        "lexical_sort_letter": "C",
                        "synopses": {
                            "small": "Featuring Dougie MacLean recorded at Blair Castle.",
                            "medium": "Music performances from the 2014 Perthshire Amber Festival, featuring Dougie MacLean recorded at Blair Castle.",
                            "large": "Ceòl bho Perthshire Amber, le òrain bho Dougie MacLean aig Caisteal Bhlàir. \n\nMusic performances from the 2014 Perthshire Amber Festival, featuring Dougie MacLean recorded at Blair Castle."
                        },
                        "tleo_id": "b04hzg2p",
                        "tleo_type": "series",
                        "images": {
                            "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p02hrnt8.jpg",
                            "type": "image"
                        },
                        "guidance": false,
                        "related_links": [],
                        "subtitle": "6. Dougie MacLean",
                        "release_date": "31 Jan 2015",
                        "parent_id": "b04hzg2p",
                        "master_brand": {
                            "id": "bbc_alba",
                            "ident_id": "p00pvbgm",
                            "attribution": "bbc_alba",
                            "titles": {
                                "small": "BBC ALBA",
                                "medium": "BBC ALBA",
                                "large": "BBC ALBA"
                            }
                        },
                        "versions": [
                            {
                                "type": "version",
                                "id": "b0511vb7",
                                "hd": false,
                                "download": true,
                                "availability": {
                                    "start": "2015-05-22T22:38:07Z",
                                    "end": "2015-06-21T21:00:00Z",
                                    "remaining": {
                                        "text": "Available for 13 days"
                                    }
                                },
                                "kind": "original",
                                "duration": {
                                    "value": "PT14M",
                                    "text": "14 mins"
                                },
                                "first_broadcast": "11:45pm 31 Jan 2015"
                            }
                        ],
                        "status": "available"
                    }
                ],
                "tleo_type": "series",
                "type": "programme_large",
                "title": "Ceòl bho Perthshire Amber",
                "synopses": {
                    "small": "Music performance from the Perthshire Amber Festival",
                    "medium": "Music performance from the Perthshire Amber Festival"
                },
                "status": "available",
                "images": {
                    "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p026qzdh.jpg",
                    "type": "image"
                },
                "lexical_sort_letter": "C",
                "master_brand": {
                    "id": "bbc_alba",
                    "ident_id": "p00pvbgm",
                    "attribution": "bbc_alba",
                    "titles": {
                        "small": "BBC ALBA",
                        "medium": "BBC ALBA",
                        "large": "BBC ALBA"
                    }
                },
                "count": 1
            },
            {
                "id": "b020tnxw",
                "initial_children": [
                    {
                        "id": "b02tgnq2",
                        "type": "episode",
                        "title": "Ceolraidh",
                        "lexical_sort_letter": "C",
                        "synopses": {
                            "small": "Ceol is orain le luchd-ciuil 's seinneadairean Gaidhlig. Gaelic music and song.",
                            "medium": "Ceol is orain le luchd-ciuil 's seinneadairean Gaidhlig. Gaelic music and song.",
                            "large": "Ceol is orain le luchd-ciuil 's seinneadairean Gaidhlig.\n\nGaelic music and song."
                        },
                        "tleo_id": "b020tnxw",
                        "tleo_type": "series",
                        "images": {
                            "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p019nf2c.jpg",
                            "type": "image"
                        },
                        "guidance": false,
                        "related_links": [],
                        "subtitle": "Episode 3",
                        "release_date": "1 Jun 2013",
                        "parent_id": "b020tnxw",
                        "master_brand": {
                            "id": "bbc_alba",
                            "ident_id": "p00pvbgm",
                            "attribution": "bbc_alba",
                            "titles": {
                                "small": "BBC ALBA",
                                "medium": "BBC ALBA",
                                "large": "BBC ALBA"
                            }
                        },
                        "versions": [
                            {
                                "type": "version",
                                "id": "b02tgj12",
                                "hd": false,
                                "download": true,
                                "availability": {
                                    "start": "2015-06-06T20:30:52Z",
                                    "end": "2015-07-06T19:25:00Z",
                                    "remaining": {
                                        "text": "Available for 28 days"
                                    }
                                },
                                "kind": "original",
                                "duration": {
                                    "value": "PT25M",
                                    "text": "25 mins"
                                },
                                "first_broadcast": "1 Jun 2013"
                            },
                            {
                                "type": "version",
                                "id": "b02tgj12",
                                "hd": false,
                                "download": true,
                                "availability": {
                                    "start": "2015-06-06T20:30:52Z",
                                    "end": "2015-07-06T19:25:00Z",
                                    "remaining": {
                                        "text": "Available for 28 days"
                                    }
                                },
                                "kind": "open-subtitled",
                                "duration": {
                                    "value": "PT25M",
                                    "text": "25 mins"
                                },
                                "first_broadcast": "1 Jun 2013"
                            }
                        ],
                        "status": "available"
                    }
                ],
                "tleo_type": "series",
                "type": "programme_large",
                "title": "Ceolraidh",
                "synopses": {
                    "small": "Gaelic music and song",
                    "medium": "Gaelic music and song"
                },
                "status": "available",
                "images": {
                    "standard": "http://ichef.bbci.co.uk/images/ic/{recipe}/p02s91zc.jpg",
                    "type": "image"
                },
                "lexical_sort_letter": "C",
                "master_brand": {
                    "id": "bbc_alba",
                    "ident_id": "p00pvbgm",
                    "attribution": "bbc_alba",
                    "titles": {
                        "small": "BBC ALBA",
                        "medium": "BBC ALBA",
                        "large": "BBC ALBA"
                    }
                },
                "count": 3
            }
        ]
    }
}
JSON;


    /**
     * Makes request to API and returns json encoded results
     *
     * @return mixed
     */
    public function get()
    {
        return json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $this->fixture));
    }
}