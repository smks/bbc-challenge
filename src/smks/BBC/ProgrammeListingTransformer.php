<?php
/**
 * Created by PhpStorm.
 * User: Shaun Stone
 * Date: 08/06/2015
 * Time: 18:00
 */

namespace Smks\BBC;

use stdClass;

/**
 * Class ListingTransformer
 * @package Smks\BBC
 */
class ProgrammeListingTransformer
{
    const MAX_PER_PAGE = 20;

    /**
     * @var stdClass $listings
     */
    private $listings;

    /**
     * @var int $totalCount
     */
    private $totalCount;
    /**
     * @var
     */
    private $character;
    /**
     * @var
     */
    private $page;
    /**
     * @var
     */
    private $programmes;

    /**
     * @var bool
     */
    private $isPaginatable = false;

    /**
     * @param stdClass $listings
     */
    public function __construct(stdClass $listings)
    {
        $this->listings = $listings;
        $this->character = $this->listings->atoz_programmes->character;
        $this->totalCount = $this->listings->atoz_programmes->count;
        $this->page = $this->listings->atoz_programmes->page;
        $this->programmes = $this->listings->atoz_programmes->elements;
        $this->isPaginatable = $this->shouldBePaginated($this->totalCount);
    }

    /**
     * @return array
     */
    public function getResults()
    {
        return [
            'character' => $this->character,
            'programmeCount' => $this->totalCount,
            'currentPage' => $this->page,
            'isPaginated' => $this->isPaginatable,
            'programmes' => $this->programmes
        ];
    }

    /**
     * @param $count
     * @return bool
     */
    private function shouldBePaginated($count)
    {
        if ($count > self::MAX_PER_PAGE) {
            return true;
        }

        return false;
    }

    /**
     * @return stdClass
     */
    public function getListings()
    {
        return $this->listings;
    }

    /**
     * @param stdClass $listings
     */
    public function setListings($listings)
    {
        $this->listings = $listings;
    }

    /**
     * @return int
     */
    public function getTotalCount()
    {
        return $this->totalCount;
    }

    /**
     * @param int $totalCount
     */
    public function setTotalCount($totalCount)
    {
        $this->totalCount = $totalCount;
    }

    /**
     * @return mixed
     */
    public function getCharacter()
    {
        return $this->character;
    }

    /**
     * @param mixed $character
     */
    public function setCharacter($character)
    {
        $this->character = $character;
    }

    /**
     * @return mixed
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param mixed $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }

    /**
     * @return mixed
     */
    public function getProgrammes()
    {
        return $this->programmes;
    }

    /**
     * @param mixed $programmes
     */
    public function setProgrammes($programmes)
    {
        $this->programmes = $programmes;
    }

    /**
     * @return boolean
     */
    public function isPaginatable()
    {
        return $this->isPaginatable;
    }

    /**
     * @param boolean $isPaginatable
     */
    public function setIsPaginatable($isPaginatable)
    {
        $this->isPaginatable = $isPaginatable;
    }
}