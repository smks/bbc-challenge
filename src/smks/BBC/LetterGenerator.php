<?php
/**
 * Created by PhpStorm.
 * User: Shaun Stone
 * Date: 08/06/2015
 * Time: 17:02
 */

namespace Smks\BBC;


class LetterGenerator
{
    const LETTERS_BEGIN = 'a';
    const LETTERS_END = 'z';
    const NUMBERS = '0-9';

    /**
     * @return LetterGenerator
     */
    public function __construct()
    {
        return $this;
    }

    /**
     * @return string
     */
    public function letterRange()
    {
        $letters = range(self::LETTERS_BEGIN, self::LETTERS_END);
        array_unshift($letters, self::NUMBERS);
        return $letters;
    }
}