<?php
/**
 *
 * {base_uri}/ibl/v1/atoz/{letter}/programmes?page={page}
 *
 * e.g.
 * https://ibl.api.bbci.co.uk/ibl/v1/atoz/a/programmes?page=1
 *
 * Created by PhpStorm.
 * User: Shaun Stone
 * Date: 08/06/2015
 * Time: 15:20
 */

namespace Smks\BBC;


class UrlBuilder
{
    /** Base URI */
    const BASE_URI = 'https://ibl.api.bbci.co.uk/';

    /** Endpoint */
    const ENDPOINT = 'ibl/v1/atoz/%s/programmes?page=%d';

    /**
     * Build a URL and return in correct form
     *
     * @param $letter
     * @param $page
     * @return string
     */
    public function build($letter, $page)
    {
        return sprintf(
            $this->getFullUri(),
            $letter,
            $page
        );
    }

    /**
     * @return string
     */
    private function getFullUri()
    {
        return self::BASE_URI . self::ENDPOINT;
    }
}