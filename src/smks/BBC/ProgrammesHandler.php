<?php
/**
 * Created by PhpStorm.
 * User: Shaun Stone
 * Date: 08/06/2015
 * Time: 15:18
 */

namespace Smks\BBC;

class ProgrammesHandler
{
    /**
     * @var UrlBuilder
     */
    private $builder;
    /**
     * @var API
     */
    private $api;

    /**
     * @param UrlBuilder $builder
     * @param API $api
     */
    function __construct(
        UrlBuilder $builder,
        API $api
    ) {
        $this->builder = $builder;
        $this->api = $api;
    }

    /**
     * Create the response and return json
     *
     * @param $letter
     * @param $page
     * @return mixed
     */
    public function fetchResults($letter, $page)
    {
        $url = $this->builder->build($letter, $page);
        return $this->api->get($url);
    }

}