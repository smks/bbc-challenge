<?php
/**
 * @author Shaun Stone
 * Date: 08/06/2015
 * Time: 10:46
 */

namespace Smks\BBC;

use Guzzle\Http\Client;
use Guzzle\Http\EntityBody;
use Guzzle\Http\Message\Request;
use Guzzle\Http\Message\Response;

class API
{
    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @param Client $httpClient
     */
    public function __construct(Client $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * Makes request to API and returns json encoded results
     * @note resolved decoding returned json special characters
     *
     * @param $url
     * @return mixed
     */
    public function get($url)
    {
        /** @var Request $request */
        $request = $this->httpClient->get($url);

        /** @var Response $response */
        $response = $request->send();

        /** var EntityBody $body */
        return json_decode(
            preg_replace(
                '/[\x00-\x1F\x80-\xFF]/',
                '',
                $response->getBody(true)
            )
        );
    }
}