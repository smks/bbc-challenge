# Instructions for Coding challenge reviewers

## Overview

I decided to use Slim to keep it as lightweight as possible.

1. logs (self explanatory).
2. public contains the bootstrap file (index.php).
3. src contains all Classes used to interface with the API, also to transform the data for use.
4. templates contains all the views.
5. tests contains unit tests, all 1 of them.
6. vendor third party libraries.

## How to test

1. ```git clone git@bitbucket.org:smks/bbc-challenge.git bbcChallenge ```
2. pull in all vendor libraries by running this in root of directory ```composer update```
3. ensure that logs/app.log has the required write/read permissions.
4. in root ```bbcChallenge/```, change into public directory ```cd public```
5. in the terminal, run: ```php -S localhost:9000```
6. open ```localhost:9000``` and away you go.

### note: server should be run from public directory

### Issue: I was forbidden to display images provided in response
e.g. 
Forbidden
You don't have permission to access /images/ic/{recipe}/p02lp96l.jpg on this server.

-----------------------------------------------

# Coding Exercise
Your team has been asked to work with a new HTTP API that provides iPlayer with lists of programmes sorted alphabetically. Your task is to use the API to generate an A-Z listing for the website.

Each of the listing pages should include:

1. a list of programme titles and images
2. the ability to paginate if the letter has more than 20 results
3. navigation to other letters

## API
The API end point follows the following format:

```
{base_uri}/ibl/v1/atoz/{letter}/programmes?page={page}
```

| Parameter     | Value                              |
| ------------- | ---------------------------------- |
| `{base_uri}`  | The base server URI, which will be provided        |
| `{page}`      | page number to retrieve, from 1    |
| `{letter}`    | can be any single letter or `0-9`  |


## Notes
* You may implement your solution in any appropriate (i.e. non-obscure) language. But please make sure your solution is working end-to-end and you include instructions for us to run and test your code.
* Your code should be available on github or similar services with a full commit history.
* Although a fully working solution is desireable your response will primarily be evaluated against four key criteria:
  * Design, Modularisation and Componentisation
  * Testing Approach and Scenarios
  * Performance
  * Style and Readability