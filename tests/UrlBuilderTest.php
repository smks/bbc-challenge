<?php
/**
 * Usually would test everything if there wasn't a time constraint :)
 *
 * Created by PhpStorm.
 * User: Shaun Stone
 * Date: 08/06/2015
 * Time: 15:35
 */

class UrlBuilderTest extends PHPUnit_Framework_TestCase {

    /**
     * Smks\BBC\UrlBuilder @var
     */
    private $subject;

    public function setUp()
    {
        $this->subject = new Smks\BBC\UrlBuilder();
    }

    public function testCorrectUrl()
    {
        $letter = 'b';
        $page = 1;

        $expected = 'https://ibl.api.bbci.co.uk/ibl/v1/atoz/b/programmes?page=1';
        $actual = $this->subject->build($letter, $page);

        $this->assertSame($expected, $actual);
    }

}
