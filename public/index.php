<?php
use JasonGrimes\Paginator;

require __DIR__ . '/../vendor/autoload.php';

// Prepare app
$app = new \Slim\Slim(
    array(
        'templates.path' => '../templates',
    )
);

// Create monolog logger and store logger in container as singleton 
// (Singleton resources retrieve the same log resource definition each time)
$app->container->singleton(
    'log',
    function () {
        $log = new \Monolog\Logger('slim-skeleton');
        $log->pushHandler(new \Monolog\Handler\StreamHandler('../logs/app.log', \Monolog\Logger::DEBUG));
        return $log;
    }
);

// Prepare view
$app->view(new \Slim\Views\Twig());
$app->view->parserOptions = array(
    'debug' => true,
    'charset' => 'utf-8',
    'cache' => realpath('../templates/cache'),
    'auto_reload' => true,
    'strict_variables' => false,
    'autoescape' => true
);
$app->view->parserExtensions = array(new \Slim\Views\TwigExtension());

/**
 * Index
 * @var Slim $app
 */
$app->get('/', function () use ($app) {

        // Sample log message
        $app->log->info("BBC Application '/' route");

        // grab the ranges we should be dealing with
        $letterGenerator = new Smks\BBC\LetterGenerator();

        $data = [];

        $data['letters'] = $letterGenerator->letterRange();

        // cache page
        $app->etag('index');

        // Render index view
        $app->render('index.twig', $data);
    }
);

/**
 * Request Programmes from API
 * @var Slim $app
 */
$app->get('/programmes/:letter/:page', function ($letter, $page) use ($app) {

        // Sample log message
        $app->log->info("BBC Request listings '/programmes' route");

        $nLGenerator = new Smks\BBC\LetterGenerator();

        $data = [];

        try {

            $programmesHandler = new Smks\BBC\ProgrammesHandler(
                new Smks\BBC\UrlBuilder(),
                new Smks\BBC\API(
                    new Guzzle\Http\Client()
                )
            );

            $results = $programmesHandler->fetchResults($letter, $page);

            /**
             * Used to mock response for testing
             *
             * $fakeApi = new Smks\BBC\APIFake();
             * $results = $fakeApi->get();
             */

            $tranformer = new Smks\BBC\ProgrammeListingTransformer($results);

            $data = $tranformer->getResults();

            $data['paginator'] = new Paginator(
                $tranformer->getTotalCount(),
                $tranformer::MAX_PER_PAGE,
                $tranformer->getPage(),
                sprintf('/programmes/%s/%s', $tranformer->getCharacter(), '(:num)')
            );

        } catch (Exception $e) {
            $data['error'] = $e->getMessage();
        }

        $data['letters'] = $nLGenerator->letterRange();

        $app->render('listing.twig', $data);
    }
);

// Run app
$app->run();
